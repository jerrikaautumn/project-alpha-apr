from django.urls import path
from accounts.views import login_page, user_logout, signup


urlpatterns = [
    path("signup/", signup, name="signup"),
    path("login/", login_page, name="login"),
    path("logout/", user_logout, name="logout"),
]
